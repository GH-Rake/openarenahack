#pragma once
#include <Windows.h>
#include <GL\GL.h>
#include <stdio.h>
//Credits c5

class GLText
{
public:
	UINT hackFont;
	bool bFontBuilt = false;
	HDC fontHDC = nullptr;

	void BuildFonts();
	void Print(float x, float y, GLubyte color[3], const char *format, ...);
};