#pragma once
#include <Windows.h>
#include "tools.h"
#include "openarenasdk.h"
#include "GLText.h"
#include "menu.h"

struct vmTable_t
{
	vm_t vm[3];
};

struct cg_entities
{
	centity_t cgents[MAX_GENTITIES];
};

//local game only
class gentity
{
public:
	int padding[0xEB8D];
	int health;
};

///// GAME SPECIFIC DEFINES  ////
#define TEAM_GAME    (OA->cgs->gametype > 2)
#define PLAYER_HEIGHT 5.3f
#define PLAYER_WIDTH 2.0f
#define PLAYER_ASPECT_RATIO PLAYER_HEIGHT / PLAYER_WIDTH
#define EYE_HEIGHT 2.0f
#define GAME_UNIT_MAGIC 4000

/// Framework Variables //////
#define VIRTUAL_SCREEN_WIDTH 800.0f
#define VIRTUAL_SCREEN_HEIGHT 600.0f

enum gameStatus_t
{
	GSTAT_LOBBY, GSTAT_GAME, GSTAT_BADMOD
};

class OA_t
{
public:

	///// GAME SPECIFIC POINTERS ////
	vmTable_t * vmTable = (vmTable_t*)0x01F65150;
	vm_t * cgame = nullptr;

	cg_entities * cgents = nullptr;
	cg_t * cg = nullptr;
	cgs_t * cgs = nullptr;
	gentity * localgent = (gentity*)*(DWORD*)0x1F651BC;

	gameStatus_t gamestatus = GSTAT_LOBBY;
	bool bLocalGame = false;

	//Parse Virtual Machine structure
	void GetVM();
	void ModScanner();

	//NativeFunctions:
	typedef char*(__cdecl * tCvar_VariableString)(const char *var_name);
	tCvar_VariableString Cvar_VariableString = (tCvar_VariableString)0x004405C8;

	typedef clipHandle_t(__cdecl * tCM_InlineModel)(int index);
	tCM_InlineModel CM_InlineModel = (tCM_InlineModel)0x00426A5C;

	typedef void(__cdecl *tCM_BoxTrace)(trace_t *results, const vec3 start, const vec3 end, vec3 mins, vec3 maxs, clipHandle_t model, int brushmask, int capsule);
	tCM_BoxTrace CM_BoxTrace = (tCM_BoxTrace)0x00437F18;
};

//Get virtual machine/mod version
void OA_t::GetVM()
{
	cg = nullptr;
	cgs = nullptr;
	cgents = nullptr;
	bLocalGame = false;
	cgame = nullptr;

	for (auto &vm : vmTable->vm)
	{
		if (strstr(vm.name, "qagame")) { bLocalGame = true; continue; }

		if (strstr(vm.name, "cgame"))
		{
			cgame = &vm;
			gamestatus = GSTAT_GAME;
			//char* gamestring = Cvar_VariableString("fs_game");

			switch (cgame->instructionCount)
			{
			case 136054: //version 88
				cgents = (cg_entities*)(cgame->dataBase + 0x1649c);
				cg = (cg_t*)(cgame->dataBase + 0xCC49C);
				cgs = (cgs_t*)(cgame->dataBase + 0xf2720);
				return;

			case 131563: //version 85
				cgents = (cg_entities*)(cgame->dataBase + 0x15EF4);
				cg = (cg_t*)(cgame->dataBase + 0xCBEF4);
				cgs = (cgs_t*)(cgame->dataBase + 0xF20EC);
				return;

			case 115964: //version 81
				cgents = (cg_entities*)(cgame->dataBase + 0x11FB4);
				cg = (cg_t*)(cgame->dataBase + 0xC7C0C);
				cgs = (cgs_t*)(cgame->dataBase + 0xEDE04);
				return;

			case 104335: //oaplus,oaster,oaweapons,furious,defrag
				cgents = (cg_entities*)(cgame->dataBase + 0x10A6C);
				cg = (cg_t*)(cgame->dataBase + 0xC6A6C);
				cgs = (cgs_t*)(cgame->dataBase + 0xECB64);///
				cgs = (cgs_t*)(cgame->dataBase + 0xA0C4); // +0xF6B70)

				//size of clientinfo_t = 0x6ac is correct 100%
				//try this in watch: (clientInfo_t*)(cgame->dataBase + 0xF6B70) should be [0]
				//clientinfo.nextstate is fucked, and need to remove timelimit & maxclients, then mapname will lineup.
				//(cgs_t*)(cgame->dataBase + 0xECAAC)
				gamestatus = GSTAT_BADMOD;
				return;

			case 159640: //missionpak
				cgents = (cg_entities*)(cgame->dataBase + 0x30838);
				cg = (cg_t*)(cgame->dataBase + 0xe6768);
				cgs = (cgs_t*)(cgame->dataBase + 0x10C960);
				gamestatus = GSTAT_BADMOD;
				return;

			default:
				gamestatus = GSTAT_BADMOD;
				return;
			}
		}
		else gamestatus = GSTAT_LOBBY;
	}
}

//Shitty offset dumper for new mods, requires modification to work
void OA_t::ModScanner() //
{
	//CGS
	byte cgspattern[] = "\x41\x4D\x44\x20\x52\x61\x64\x65\x6F\x6E\x20\x48\x44\x20\x37\x38\x30\x30";
	byte* cgsTemp = Mem::FindPattern((byte*)cgspattern, sizeof(cgspattern) - 1, (cgame->dataBase), (cgame->dataBase + 0x001FFFFF));
	cgs = (cgs_t*)(cgsTemp - 0x4e84);
	DWORD cgs_offet = (DWORD)cgs - (DWORD)cgame->dataBase;

	//CG
	GLint tempviewport[4] = { 0 };
	glGetIntegerv(GL_VIEWPORT, tempviewport);
	byte viewportpattern[8];
	memcpy(viewportpattern, &tempviewport[2], 8);
	std::vector<byte*> cgtemps = Mem::FindPatterns(viewportpattern, 8, (cgame->dataBase), (cgame->dataBase + 0x001FFFFF));
	cg = (cg_t*)(cgtemps[0] - 0x1a9fc);
	DWORD cg_offset = (DWORD)cg - (DWORD)cgame->dataBase;

	//cg_entities
	byte cgpattern[] = "\x02\x00\x00\x00\x01\x00\x00\x00\x04\x00\x00\x00\x01\x00\x00\x00";
	std::vector<byte*> cgentstemps = Mem::FindPatterns((byte*)cgpattern, sizeof(cgpattern) - 1, (cgame->dataBase), (cgame->dataBase + 0xC261B0));
	if (!cgentstemps.empty())
	{
		cgents = (cg_entities*)(cgentstemps[0] - (0x2D8 * 2));
		DWORD cgent_offset = (DWORD)cgents - (DWORD)cgame->dataBase;
	}
	int x = 5;
}

OA_t * OA = nullptr;

class player
{
public:
	centity_t * ent;
	clientInfo_t * client;
	vec3 screen;
	float dist;
	float fDistFromCross;

	//default constructor
	player() {}

	//contructor
	player(centity_t * p)
	{
		ent = p;
		client = &OA->cgs->clientinfo[p->currentState.clientNum];
		dist = OA->cg->activeSnapshots[0].ps.origin.dist(p->currentState.pos.trBase);
		fDistFromCross = 3.4028e+38f;
	}

	bool bValid();
	bool bEnemy();

	bool WorldToScreen(refdef_t refdef);

	bool bVisible();
};

//Validation local player objects
bool player::bValid()
{
	//if (cgents->cgents[i].currentValid == qtrue)

	//if not a player
	if (ent->currentState.eType != ET_PLAYER) return false;

	//if local player
	if (ent->currentState.number == OA->cg->clientNum) return false;

	//if spectator
	if (client->team == TEAM_SPECTATOR) return false;
	//if not valid
	if (client->infoValid == qfalse) return false;

	//if current state not valid - ESP won't get drawn sometimes when far away
	if (ent->currentValid == qfalse)

		//if dead
		if (ent->currentState.eFlags & EF_DEAD) return false;
	if (ent->currentState.legsAnim == BOTH_DEATH1) return false;
	if (ent->currentState.legsAnim == BOTH_DEATH2) return false;
	if (ent->currentState.legsAnim == BOTH_DEATH3) return false;
	if (ent->currentState.legsAnim == BOTH_DEAD1) return false;
	if (ent->currentState.legsAnim == BOTH_DEAD2) return false;
	if (ent->currentState.legsAnim == BOTH_DEAD3) return false;

	return true;
}

bool player::bEnemy()
{
	if (TEAM_GAME && OA->cgs->clientinfo[OA->cg->clientNum].team == client->team)
	{
		return false;
	}
	else return true;
}

//Validation check for entities in memory
bool bValid(int i)
{
	//if (cgents->cgents[i].currentValid == qtrue)

	//if not a player
	if (OA->cgents->cgents[i].currentState.eType != ET_PLAYER) return false;

	//if local player
	if (OA->cgents->cgents[i].currentState.number == OA->cg->clientNum) return false;

	//if spectator
	if (OA->cgs->clientinfo[OA->cgents->cgents[i].currentState.number].team == TEAM_SPECTATOR) return false;
	//if not valid
	if (OA->cgs->clientinfo[OA->cgents->cgents[i].currentState.number].infoValid == qfalse) return false;

	//if current state not valid - ESP won't get drawn sometimes when far away
	if (OA->cgents->cgents[i].currentValid == qfalse)

		//if dead
		if (OA->cgents->cgents[i].currentState.eFlags & EF_DEAD) return false;
	if (OA->cgents->cgents[i].currentState.legsAnim == BOTH_DEATH1) return false;
	if (OA->cgents->cgents[i].currentState.legsAnim == BOTH_DEATH2) return false;
	if (OA->cgents->cgents[i].currentState.legsAnim == BOTH_DEATH3) return false;
	if (OA->cgents->cgents[i].currentState.legsAnim == BOTH_DEAD1) return false;
	if (OA->cgents->cgents[i].currentState.legsAnim == BOTH_DEAD2) return false;
	if (OA->cgents->cgents[i].currentState.legsAnim == BOTH_DEAD3) return false;

	return true;
}

//WorldToScreen wrapper for quake3 engine
bool player::WorldToScreen(refdef_t refdef)
{
	vec3 chest = ent->currentState.pos.trBase;
	chest.z += 7;
	return chest.WorldToScreen2(refdef.fov_x, refdef.fov_x, refdef.width, refdef.height, refdef.viewaxis[0], refdef.viewaxis[1], refdef.viewaxis[2], refdef.vieworg, screen);
}

bool player::bVisible()
{
	trace_t result;

	//clipHandle_t modelhandle = OA->CM_InlineModel(ent->currentState.modelindex);
	int *fuck = (int*)0xE4BFB8;
	OA->CM_BoxTrace(&result, OA->cg->refdef.vieworg, ent->currentState.apos.trBase, vec3_origin, vec3_origin, 0, MASK_SOLID, 0);

	if (result.fraction != 1.0f)//change it faggot
	{
		return true;
	}
	else return false;
}
std::vector<player> players;