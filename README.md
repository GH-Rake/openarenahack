# OpenArena Aimbot / ESP

![57JuzC4[3].png](https://bitbucket.org/repo/B4pAkG/images/2445384272-57JuzC4%5B3%5D.png)

## What does this do?

This is an internal hack with these features:
Aimbot
OpenGL ESP

## Why?

This was the next step on my coding/reversing journey.  I ported my assault cube multihack source code over to this Quake3 Engine game.  Quake3 Engine required alot more reversing because it uses a virtual machine interpreter.  I spent a lot of time optimizing the code and applying knew knowledge to this project.  I needed some help and we put together the first GuidedHacking Community Project with help from community members.

## Usage

Compile.

Execute the hack and the game, the hack will be automatically injected.
[F1] Aimbot
[F2] ESP
[Right Mouse Button] Aim/Shoot
If you want to switch targets, let go of right click and press it again. You are meant to only press right click when you want to shoot the target nearest to your crosshair.

## TODO
Nothing.  This project is retired.

## Development History
Ported my assault cube multihack framework to this game
Created GH Community Project
Added loader and GUI
Optimized & cleaned it up
Created a youtube video explaining the code

###

[Open Arena Hack Thread](http://guidedhacking.com/showthread.php?7908-OpenArena-ESP-Hack-amp-Aimbot-Source-Code-Download)

[Open Arena Hack Video 1](https://www.youtube.com/watch?v=O0ivDI32rTc)

[Open Arena Hack Video 2](https://youtu.be/LT4n0KiSA90)

## Credits

Rake, Mambda, Kilo, Broihon