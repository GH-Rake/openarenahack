#pragma once
#include <windows.h>
#include "gltext.h"
#include "gldraw.h"
#include "menu.h"
#include "reversals.h"
#include <vector>

namespace ESP
{
	void DrawBox(player p, vec3 screen, GLText * glText);
	void ESP(std::vector<player> players, GLText * glText);
};

bool bESP = false;

void ESP::DrawBox(player p, vec3 screen, GLText * glText)
{
	GLubyte * color = nullptr;
	if (p.bEnemy())
	{
		color = red;
	}
	else color = green;

	float scale = (GAME_UNIT_MAGIC / p.dist) * (WINDOWWIDTH / VIRTUAL_SCREEN_WIDTH);
	float x = screen.x - scale;
	float y = screen.y - scale * PLAYER_ASPECT_RATIO;
	float width = scale * 2;
	float height = scale * PLAYER_ASPECT_RATIO * 2;

	GL::DrawOutline(x, y, width, height, 2.0f, color);

	float textX = GL::centerText(x, width, (float)strlen(p.client->name) * MENU_FONT_WIDTH);
	float textY = y - MENU_FONT_HEIGHT / 2;
	glText->Print(textX, textY, color, "%s", p.client->name);
}

//loop through players
void ESP::ESP(std::vector<player> players, GLText * glText)
{
	for (auto p : players)
	{
		vec3 center = p.ent->currentState.pos.trBase;
		center.z -= EYE_HEIGHT + PLAYER_HEIGHT / 2;

		if (p.WorldToScreen(OA->cg->refdef))
		{
			DrawBox(p, p.screen, glText);
		}
	}
}