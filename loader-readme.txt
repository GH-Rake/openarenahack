ESP & Aimbot

IMPORTANT:
In game, go to Setup->Controls->Move and disable the right mouse button keybind for "sidestep / turn"

Features not included:
Visibility Check, Ballistic Projectile Compensation

Supported Mods:
version 81
version 85
version 88
Cookies
City
CTF
1v1

Known Unsupported Mods:
furious, oaster, oaplus, oaweapons, defrag