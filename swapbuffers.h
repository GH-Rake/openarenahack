#pragma once
#include "reversals.h"
#pragma comment(lib, "lib\\detours.lib")
#include "include\detours.h"
#include "gldraw.h"
#include "esp.h"
#include "aimbot.h"

//Hooking SDL wrapper so video capture software can hook GLSwapBuffers
typedef void(__stdcall * tSDL_GL_SwapBuffers)();
tSDL_GL_SwapBuffers oSDL_GL_SwapBuffers;

GLText * glText = new GLText();
extern Aimbot * aimbot;

void __stdcall hSDL_GL_SwapBuffers()
{
	//Get Window size
	glGetFloatv(GL_VIEWPORT, viewport);

	HDC currentHDC = wglGetCurrentDC();

	if (!glText->bFontBuilt || currentHDC != glText->fontHDC) { glText->BuildFonts(); }

	if (!menu || !menu->bMenuBuilt)
	{
		menu = new Menu;
		menu->BuildMenu();

		menuError = new Menu;
		menuError->BuildError();

		menu->bMenuBuilt = true;
	}

	//Parse Virtual Machine
	OA->GetVM();

	if (OA->gamestatus == GSTAT_GAME)
	{
		aimbot->ReadPlayerData();

		if (bESP)
		{
			GL::SetupOrtho();
			ESP::ESP(players, glText);
			GL::RestoreGL();
		}

		if (OA->bLocalGame)
		{
			OA->localgent->health = 999;
		}
	}

	aimbot->ReadHotKeys();

	if (OA->gamestatus == GSTAT_BADMOD)
	{
		menuError->Draw(glText);
	}
	else if (bMenu)
	{
		menu->Draw(glText);
	}

	return oSDL_GL_SwapBuffers();
}

void hookSDLSwapBuffers()
{
	HMODULE hMod = GetModuleHandle(L"SDL.dll");
	if (hMod)
	{
		oSDL_GL_SwapBuffers = (tSDL_GL_SwapBuffers)(DWORD)GetProcAddress(hMod, "SDL_GL_SwapBuffers");
		DetourTransactionBegin();
		DetourUpdateThread(GetCurrentThread());
		DetourAttach(&(PVOID &)oSDL_GL_SwapBuffers, hSDL_GL_SwapBuffers);
		DetourTransactionCommit();
	}
}