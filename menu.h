#pragma once
#include "gldraw.h"
#include <vector>
//TODO: remove externs and re-write BuildMenu

float fWindowScale;
extern bool bAimbot;
extern bool bESP;
bool bMenu = true;

#define MENU_FONT_HEIGHT 15
#define MENU_FONT_WIDTH 9
#define MENU_ROW_YBUFFER 3
#define MENU_ROW_XBUFFER (MENU_FONT_WIDTH * 2)
#define MENU_LINE_WIDTH 2
#define MENU_FONT_COLOR red
#define MENU_OUTLINE_COLOR black

enum rowtype_t { M_FUNCTION, M_HEADER, M_TAB, M_FOOTER };

class row;

class Menu
{
public:
	bool bMenuBuilt = false;
	float width;
	float height;
	float x, y;
	//int numTabs;
	float nextRowY;
	int longRowStrLen;
	std::vector<row> rows;

	void BuildMenu();
	void BuildError();
	void GetMenuWidth();
	void GetLongestFunction();
	void Draw(GLText * glText);
};

class row
{
public:
	enum rowtype_t type;
	char * text;
	char * text2;
	bool * status;
	float height;
	Menu * pmenu;

	row(Menu * pmenu, rowtype_t type, char* text, bool * status);
	void Draw(GLText * glText);
};

class tab
{
	//
};

void Menu::GetMenuWidth()
{
	longRowStrLen = 0;
	for (auto r : rows)
	{
		int len;
		if (r.type == M_FUNCTION && r.status != nullptr)
		{
			len = strlen(r.text) + strlen(r.text2);
		}
		else
		{
			len = strlen(r.text);
		}

		if (len > longRowStrLen)
		{
			longRowStrLen = len;
		}
	}
	width = (float)(longRowStrLen * MENU_FONT_WIDTH) + (MENU_ROW_XBUFFER);
}

void Menu::BuildMenu()
{
	rows.push_back(row(this, M_HEADER, "[GH] Community Hack", nullptr));
	rows.push_back(row(this, M_FUNCTION, "[F1] Aimbot :  ", &bAimbot));
	rows.push_back(row(this, M_FUNCTION, "[F2] ESP    :  ", &bESP));
	rows.push_back(row(this, M_FUNCTION, "[INS]Hide Menu", nullptr));
	rows.push_back(row(this, M_FOOTER, "GuidedHacking.com", nullptr));

	GetMenuWidth();
	x = WINDOWWIDTH * .65f;
	y = WINDOWHEIGHT * .01f;
	height = 0;
	nextRowY = y;
}

void Menu::BuildError()
{
	rows.push_back(row(this, M_HEADER, "[GH] Community Hack", nullptr));
	rows.push_back(row(this, M_FUNCTION, "Incompatible Mods", nullptr));
	rows.push_back(row(this, M_FUNCTION, "furious,oaster,", nullptr));
	rows.push_back(row(this, M_FUNCTION, "oaplus,oaweap,defrag", nullptr));
	rows.push_back(row(this, M_FOOTER, "GuidedHacking.com", nullptr));

	GetMenuWidth();
	x = WINDOWWIDTH * .65f;
	y = WINDOWHEIGHT * .01f;
	height = 0;
	nextRowY = y;
}

void Menu::Draw(GLText * glText)
{
	GL::SetupOrtho();
	for (row r : rows)
	{
		r.Draw(glText);
		nextRowY += r.height;
		r.pmenu->height += r.height;
	}

	GL::DrawOutline(x, y, width, height, MENU_LINE_WIDTH, black);
	GL::RestoreGL();
	height = 0;
	nextRowY = y;
}

row::row(Menu * pmenu, rowtype_t type, char* text, bool * status)
{
	this->pmenu = pmenu;
	this->type = type;
	this->text = text;
	this->status = status;

	this->height = MENU_FONT_HEIGHT + (MENU_ROW_YBUFFER * 2);

	if (type == M_FOOTER || type == M_HEADER)
	{
		this->height += MENU_LINE_WIDTH / 2;
	}

	else if (status != nullptr)
	{
		if (*status)
		{
			text2 = "ON";
		}
		else text2 = "OFF";
	}
}

void row::Draw(GLText * glText)
{
	GL::DrawFilledRect(pmenu->x, pmenu->nextRowY, pmenu->width, height, gray);

	if (type == M_FOOTER || type == M_HEADER)
	{
		GL::DrawOutline(pmenu->x, pmenu->nextRowY, pmenu->width, height, MENU_LINE_WIDTH, black);
		pmenu->nextRowY += MENU_LINE_WIDTH / 2;
	}

	if (type == M_FUNCTION)
	{
		vec3 pos = GL::centerText(pmenu->x, pmenu->nextRowY, pmenu->width, height, pmenu->width - MENU_ROW_XBUFFER, MENU_FONT_HEIGHT);

		if (status != nullptr)
		{
			if (*status)
			{
				text2 = "ON";
			}
			else text2 = "OFF";
			glText->Print(pos.x, pos.y, MENU_FONT_COLOR, "%s%s", text, text2);
		}
		else glText->Print(pos.x, pos.y, MENU_FONT_COLOR, "%s", text, text2);
	}

	else
	{
		vec3 pos = GL::centerText(pmenu->x, pmenu->nextRowY, pmenu->width, height, (float)strlen(text) * MENU_FONT_WIDTH, MENU_FONT_HEIGHT);
		glText->Print(pos.x, pos.y, MENU_FONT_COLOR, "%s", text);
	}
}

Menu * menu = new Menu();
Menu * menuError = nullptr;