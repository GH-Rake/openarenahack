#pragma once
#pragma comment(lib, "opengl32.lib")
#include <windows.h>
#include <gl\GL.h>
#include "geom.h"

GLfloat viewport[4] = { 0 };
#define WINDOWWIDTH viewport[2]
#define WINDOWHEIGHT viewport[3]
GLubyte red[3] = { 255, 0, 0 };
GLubyte green[3] = { 0, 255, 0 };
GLubyte gray[3] = { 55, 55, 55 };
GLubyte lightgray[3] = { 192, 192, 192 };
GLubyte black[3] = { 0, 0, 0 };

namespace GL
{
	void SetupOrtho();
	void RestoreGL();

	//center on X and Y axes
	vec3 centerText(float x, float y, float width, float height, float textWidth, float textHeight);

	//center on X axis only
	float centerText(float x, float width, float textWidth);

	void DrawFilledRect(float x, float y, float width, float height, GLubyte color[3]);
	void DrawOutline(float x, float y, float width, float height, float lineWidth, GLubyte color[3]);
};

void GL::SetupOrtho()
{
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, WINDOWWIDTH, WINDOWHEIGHT, 0, -1.0f, 1.0f);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
}

void GL::RestoreGL()
{
	glPopAttrib();
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
}

vec3 GL::centerText(float x, float y, float width, float height, float textWidth, float textHeight)
{
	vec3 text;
	text.x = x + (width - textWidth) / 2;
	text.y = y + textHeight;
	return text;
}

float GL::centerText(float x, float width, float textWidth)
{
	if (width > textWidth)
	{
		float difference = width - textWidth;
		return (x + (difference / 2));
	}

	else
	{
		float difference = textWidth - width;
		return (x - (difference / 2));
	}
}

void GL::DrawFilledRect(float x, float y, float width, float height, GLubyte color[3])
{
	glColor3ub(color[0], color[1], color[2]);
	glBegin(GL_QUADS);
	glVertex2f(x, y);
	glVertex2f(x + width, y);
	glVertex2f(x + width, y + height);
	glVertex2f(x, y + height);
	glEnd();
}

void GL::DrawOutline(float x, float y, float width, float height, float lineWidth, GLubyte color[3])
{
	glLineWidth(lineWidth);
	glBegin(GL_LINE_STRIP);
	glColor3ub(color[0], color[1], color[2]);
	glVertex2f(x - 0.5f, y - 0.5f);
	glVertex2f(x + width + 0.5f, y - 0.5f);
	glVertex2f(x + width + 0.5f, y + height + 0.5f);
	glVertex2f(x - 0.5f, y + height + 0.5f);
	glVertex2f(x - 0.5f, y - 0.5f);
	glEnd();
}
