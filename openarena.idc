#include <idc.idc>

static RenameSegs(address, name)
{
	if (name == 0x75)
	{
		RenameSeg(address, "VM_UI");
		print("VM_UI Renamed");
		return;
	}

	else if (name == 0x63)
	{
		RenameSeg(address, "VM_CGAME");
		print("VM_CGAME Renamed");
		return;
	}

	else if (name == 0x71)
	{
		RenameSeg(address, "VM_QAGAME");
		print("VM_QAGAME Renamed");
		print("VM_QAGAME Found = Local Game");
		return;
	}

	else print("No VM_QAGAME Found = Multiplayer");
}

static FindCodeAndFunctions(address, codelength)
{
	auto start = SegStart(address);
	auto endCode = start + codelength;
	auto current = start;
	auto s = sprintf("SegmentStart = 0x%X", start);
	print(s);
	s = sprintf("SegmentEnd = 0x%X", endCode);
	print(s);

	while (current <= endCode)
	{

		auto length = MakeCode(current);
		if (length > 0)
		{
			//s = sprintf("Coderized 0x%X", current);
			//print(s);
			//s = sprintf("Length = 0x%X", length);
			//print(s);
			current = current + length;
		}
		else
		{
			//s = sprintf("MakeCode Failed @ 0x%X", current);
			//print(s);
			current = current + 0x1;
		}
	}
	print("Coderization Complete");

	current = start;
	while (current < endCode)
	{  
		auto function = FindBinary(current, SEARCH_DOWN, "81 EE ?? 00 00 00 83 C7 04");

		if (function == BADADDR)
		{
			print("No Function Patterns Found");
			return;
		}

		if (function > endCode)
		{
			print("Functionator Complete");
			return;
		}

		else
		{
			auto ret = FindBinary(function, SEARCH_DOWN, "C3");
			auto success = MakeFunction(function, ret);

			if (success == 0)
			{
				s = sprintf("MakeFunction Failed @ 0x%X", function);
				print(s);
				current = function + 1;
			}
			if (success != 0)
			{
				s = sprintf("Function Created @ 0x%X", function);
				print(s);
				current = function + 1;
			}
		}
	}
}


static main()
{
	auto vm0 = 0x01F65150;
	auto vm0name = Byte(vm0 + 0x08);
	auto vm0codeBase = Dword(vm0 + 0x5c);
	auto vm0CodeLength = Dword(vm0 + 0x60);
	RenameSegs(vm0codeBase, vm0name);

	auto vm1 = vm0 + 0x94;
	auto vm1name = Byte(vm1 + 0x08);
	auto vm1codeBase = Dword(vm1 + 0x5c);
	auto vm1codeLength = Dword(vm1 + 0x60);
	RenameSegs(vm1codeBase, vm1name);

	auto vm2 = vm1 + 0x94;
	auto vm2name = Byte(vm2 + 0x08);
	auto vm2codeBase = Dword(vm2 + 0x5c);
	auto vm2codeLength = Dword(vm2 + 0x60);
	RenameSegs(vm2codeBase, vm2name);

	FindCodeAndFunctions(vm0codeBase, vm0CodeLength);
	FindCodeAndFunctions(vm1codeBase, vm1codeLength);
	//Uncomment to disassemble local games
	//FindCodeAndFunctions(vm2codeBase, vm2codeLength);
	return 0;
}