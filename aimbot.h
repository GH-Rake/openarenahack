#pragma once
bool bAimbot = false;

//sort algorithm predicate
bool SortByAngle(player *lhs, player *rhs)
{
	if (abs(lhs->fDistFromCross - rhs->fDistFromCross) < 100)
	{
		return lhs->dist < rhs->dist;
	}
	else  return lhs->fDistFromCross < rhs->fDistFromCross;
}

class Aimbot
{
public:
	bool bTargetLock = false;
	player * target;

	void MoveMouse(vec3 point);
	void GetTarget();
	void ReadPlayerData();
	void ReadHotKeys();
};

void Aimbot::ReadPlayerData()
{
	players.clear();

	for (int i = 0; i < MAX_CLIENTS; i++)
	{
		if (bValid(i))
		{
			players.push_back(player((&OA->cgents->cgents[i])));
		}
	}
}

void Aimbot::MoveMouse(vec3 point)
{
	INPUT Input = { 0 };
	Input.type = INPUT_MOUSE;
	Input.mi.dwFlags = MOUSEEVENTF_MOVE;
	Input.mi.dx = point.x - WINDOWWIDTH / 2;
	Input.mi.dy = point.y - WINDOWHEIGHT / 2;
	SendInput(1, &Input, sizeof(INPUT));
}

void Aimbot::GetTarget()
{
	//if target is locked, don't get new target
	if (target && bTargetLock)
	{
		if (target->bValid())
		{
			if (target->WorldToScreen(OA->cg->refdef))
			{
				MoveMouse(target->screen);
				return;
			}
			else bTargetLock = false;
		}
	}

	std::vector <player *> targets;
	target = nullptr;

	for (auto &p : players)
	{
		if (!p.bEnemy()) continue;
		if (!p.bVisible()) continue;

		vec3 crosshair = { WINDOWWIDTH / 2, WINDOWHEIGHT / 2 };

		//only add to targets if on screen
		if (p.WorldToScreen(OA->cg->refdef))
		{
			p.fDistFromCross = crosshair.Get2DDist(p.screen);

			targets.push_back(&p);
		}
	}

	sort(targets.begin(), targets.end(), SortByAngle);

	if (!targets.empty())
	{
		target = targets[0];
		bTargetLock = true;
		MoveMouse(target->screen);
	}
}

void Aimbot::ReadHotKeys()
{
	if (GetFocus() == FindWindowA(0, "OpenArena"))
	{
		//if key is not pressed, target unlock
		if (!(GetAsyncKeyState(VK_RBUTTON)) & 1)
		{
			bTargetLock = false;
		}

		if (GetAsyncKeyState(VK_RBUTTON) && bAimbot && (OA->gamestatus == GSTAT_GAME))
		{
			GetTarget();
		}

		if (GetAsyncKeyState(VK_F1) & 1)
		{
			bAimbot = !bAimbot;
		}

		if (GetAsyncKeyState(VK_F2) & 1)
		{
			bESP = !bESP;
		}

		if (GetAsyncKeyState(VK_INSERT) & 1)
		{
			bMenu = !bMenu;
		}
	}
}

Aimbot * aimbot = new Aimbot();