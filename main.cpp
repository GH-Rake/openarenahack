//GuidedHacking.com
//Big Thanks to Solaire, Broihon, mambda, c5, Kilo, Nazalas and everyone at GH
#include "swapbuffers.h"
#include <thread>

//Seperate thread needed when calling Sleep()
void shootThread()
{
	INPUT leftMouseShoot = { 0 };
	while (1)
	{
		if (aimbot->target && bAimbot && GetAsyncKeyState(VK_RBUTTON))
		{
			//press
			leftMouseShoot.type = INPUT_MOUSE;
			leftMouseShoot.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
			SendInput(1, &leftMouseShoot, sizeof(INPUT));

			std::this_thread::sleep_for(std::chrono::milliseconds(5));

			// Release
			leftMouseShoot.mi.dwFlags = MOUSEEVENTF_LEFTUP;
			SendInput(1, &leftMouseShoot, sizeof(INPUT));
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}

DWORD __stdcall startHack(LPVOID param)
{
	OA = new OA_t;

	hookSDLSwapBuffers();
	std::thread shootThread(shootThread);
	shootThread.detach();
	return 0;
}

BOOL __stdcall DllMain(HINSTANCE hModule, DWORD dwReason, LPVOID lpReserved)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		CreateThread(nullptr, 0, startHack, nullptr, 0, nullptr);
		DisableThreadLibraryCalls(hModule);
		break;

	case DLL_PROCESS_DETACH:
		break;

	default:
		break;
	}
	return true;
}