#pragma once
#include <windows.h>
#include <vector>

namespace Mem
{
	//Doesn't work if your pattern contains 0xff
	byte* FindPattern(byte pattern[], int length, byte* begin, byte* end);
	//Find all matching patterns
	std::vector<byte*> FindPatterns(byte pattern[], int length, byte* begin, byte* end);
};