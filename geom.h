#pragma once
#define PI ( 3.1415927f )
#include <algorithm>

//vector and matrix classes modified from assaultcube source
//Lots of unnecesary stuff

struct vec3
{
	float x, y, z;

	vec3 &mul(float f) { x *= f; y *= f; z *= f; return *this; }
	vec3 &div(float f) { x /= f; y /= f; z /= f; return *this; }
	vec3 &add(float f) { x += f; y += f; z += f; return *this; }
	vec3 &sub(float f) { x -= f; y -= f; z -= f; return *this; }

	vec3 &add(const vec3 &o) { x += o.x; y += o.y; z += o.z; return *this; }
	vec3 &sub(const vec3 &o) { x -= o.x; y -= o.y; z -= o.z; return *this; }

	float squaredlen() const { return x * x + y * y + z * z; }
	float sqrxy() const { return x * x + y * y; }

	float dot(const vec3 &o) const { return x * o.x + y * o.y + z * o.z; }
	float dotxy(const vec3 &o) const { return x * o.x + y * o.y; }

	float magnitude() const { return sqrtf(squaredlen()); }
	vec3 &normalize() { div(magnitude()); return *this; }

	float dist(const vec3 &e) const { vec3 t; return dist(e, t); }
	float dist(const vec3 &e, vec3 &t) const { t = *this; t.sub(e); return t.magnitude(); }

	float Get2DDist(const vec3 &e) const { float dx = e.x - x, dy = e.y - y; return sqrtf(dx*dx + dy * dy); }

	vec3 CalcAngle(vec3 dst)
	{
		vec3 angles;
		angles.x = (-(float)atan2(dst.x - x, dst.y - y)) / PI * 180.0f + 180.0f;
		angles.y = (atan2(dst.z - z, this->dist(dst))) * 180.0f / PI;
		angles.z = 0.0f;
		return angles;
	}

	vec3 scaleFixedPoint(float scalex, float scaley, vec3 fixedPoint)
	{
		vec3 newvec;
		newvec.x = x * scalex + fixedPoint.x*(1 - scalex);
		newvec.y = y * scaley + fixedPoint.y*(1 - scaley);
		return newvec;
	}

	bool WorldToScreen2(float fovx, float fovy, float windowWidth, float windowHeight, vec3 left, vec3 up, vec3 forward, vec3 origin, vec3 &screen)
	{
		vec3 transform;
		float xc, yc;
		float px, py;
		float z;

		px = tan(fovx * PI / 360.0f);
		py = tan(fovy * PI / 360.0f);

		transform = this->sub(origin); //this = destination

		xc = windowWidth / 2.0f;
		yc = windowHeight / 2.0f;

		z = transform.dot(left);
		if (z <= 0.1) { return false; }

		screen.x = xc - transform.dot(up) *xc / (z*px);
		screen.y = yc - transform.dot(forward) *yc / (z*py);
		return true;
	}
};