#pragma once
#include "tools.h"

byte* Mem::FindPattern(byte pattern[], int length, byte* begin, byte* end)
{
	int i = 0;
	for (byte* current = begin; current < end; current++)
	{
		//if pattern matches 100%
		if (i == length) return (current - length);

		//if wildcard or matching pattern
		if (pattern[i] == 0xff || *current == pattern[i]) i++;
		else i = 0; //if no match reset pattern
	}
	return nullptr;
}

std::vector<byte*> Mem::FindPatterns(byte pattern[], int length, byte* begin, byte* end)
{
	byte* current = begin;
	std::vector<byte*> matches;

	while (current < end - length)
	{
		byte* temp = FindPattern(pattern, length, current, end);
		if (temp == nullptr) return matches;
		else
		{
			matches.push_back(temp);
			current = temp;
			current++;
		}
	}
	return matches;
}